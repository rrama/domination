package me.rrama.Domination;

import java.util.Arrays;
import me.rrama.Domination.Events.DeathAndRespawnEvent;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.Timer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class TimedTasks {
    
    public static int stage;
    public static BukkitTask TimeTask, ScoreAddTask, CaptureTask, RegenTask;
    
    public static void TimerMain() {
        if (RramaGaming.gameInPlay.equals("Domination") && RramaGaming.R == RoundState.InGame) {
            stage = 0;
            new Timer(Domination.This, false, 485);
            Time();
            ScoreAdderTimer();
            CallCapture();
            CallRegen();
        } else {
            Bukkit.broadcastMessage(Domination.This.TagB + "TimerMain() was launched when Domination was not in play or R != InGame");
        }
    }
    
    public static void Time() {
        TimeTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Domination.This, new Runnable() {

            @Override
            public void run() {
                if (RramaGaming.gameInPlay.equals("Domination") && RramaGaming.R == RoundState.InGame && Domination.This.timer.isRunning()) {
                    stage++;
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            for (String InheritanceS : Domination.Classes.get((String)G.getMetaData("Domination-Class")).getInheritance()) {
                                G.getPlayer().getInventory().addItem(Domination.Classes.get(InheritanceS).getUpgradeItem());
                            }
                        }
                    }
                } else {
                    TimeTask.cancel();
                }
            }
        }, 2405, 2400);
    }
    
    public static void ScoreAdderTimer() {
        ScoreAddTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Domination.This, new Runnable() {

            @Override
            public void run() {
                if (Domination.This.timer.isRunning()) {
                    for (Checkpoint C : Arrays.asList(Checkpoints.A, Checkpoints.B, Checkpoints.C)) {
                        Team T = C.getTeamInPosetion();
                        if (T != null) {
                            T.ScoreAdd1();
                        }
                    }
                } else {
                    ScoreAddTask.cancel();
                }
            }
        }, 100, 100);
    }
    
    public static void CallCapture() {
        CaptureTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Domination.This, new Runnable() {

            @Override
            public void run() {
                if (Domination.This.timer.isRunning()) {
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            Player P = G.getPlayer();
                            P.setExp(0);
                            if (DeathAndRespawnEvent.Kills.containsKey(G.getName())) {
                                P.setLevel(DeathAndRespawnEvent.Kills.get(G.getName()));
                            } else {
                                P.setLevel(0);
                            }
                        }
                    }
                    for (Checkpoint C : Arrays.asList(Checkpoints.A, Checkpoints.B, Checkpoints.C)) {
                        C.clearPlayersInbound();
                        for (Gamer G : RramaGaming.getGamers()) {
                            if (G.m()) {
                                Player P = G.getPlayer();
                                if (P.getLocation().toVector().distanceSquared(C.getPostion()) < 16 && G.isAlive()) { //Squared requires less CPU.
                                    C.addPlayerToInbound(G.getName());
                                }
                            }
                        }
                        C.CappingCheck();
                    }
                } else {
                    CaptureTask.cancel();
                }
            }
            
        }, 100, 20); //Delay to allow both teams to get to their first checkpoint.
    }
    
    public static void CallRegen() {
        RegenTask = Bukkit.getScheduler().runTaskTimer(Domination.This, new Runnable() {

            @Override
            public void run() {
                if (Domination.This.timer.isRunning()) {
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            Player P = G.getPlayer();
                            if (G.isAlive()) {
                                try {
                                    P.setHealth(P.getHealth()+1);
                                } catch (IllegalArgumentException ex) {}
                            }
                        }
                    }
                } else {
                    RegenTask.cancel();
                }
            }
            
        }, 40, 40);
    }
}