package me.rrama.Domination;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.map.MapCursor;

public class Team {
    
    final public ArrayList<String> Members;
    final public ArrayList<String> BetratyingMembers = new ArrayList<>();
    private int Score = 0;
    final private String Name;
    final private ChatColor TeamColour;
    final private MapCursor.Type PointerColour;
    final private DyeColor WoolColour;
    
    public Team(final String Name, final ChatColor TeamColour, final MapCursor.Type PointerColour, final ArrayList<String> Members) {
        this.Name = Name;
        this.TeamColour = TeamColour;
        this.PointerColour = PointerColour;
        WoolColour = DyeColor.valueOf(TeamColour.name().replaceFirst("DARK_", ""));
        this.Members = Members;
    }
    
    public int getScore() {
        return Score;
    }
    
    public void setScore(final int Score) {
        this.Score = Score;
    }
    
    public void ScoreAdd1() {
        ++Score;
    }
    
    public String getName() {
        return Name;
    }
    
    public ChatColor getTeamColour() {
        return TeamColour;
    }
    
    public MapCursor.Type getPointerColour() {
        return PointerColour;
    }
    
    public DyeColor getTeamWoolColour() {
        return WoolColour;
    }
    
    public void sendMessage(String message) {
        for (String s : Members) {
            Bukkit.getPlayerExact(s).sendMessage(message);
        }
    }
}
