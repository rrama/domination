package me.rrama.Domination.Events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import me.rrama.Domination.*;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DeathAndRespawnEvent implements Listener {
    
    public static HashMap<String, Integer> Kills = new HashMap<>();
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player P = event.getEntity();
        String PN = P.getName();
        if (RramaGaming.gameInPlay.equals("Domination") && RramaGaming.getGamer(PN).m()) {
            event.setDroppedExp(0);
            event.getDrops().clear();
            Player Attacker = null;
            for (String S : Domination.Red.Members) {
                if (event.getDeathMessage().contains(" " + S)) Attacker = Bukkit.getPlayer(S);
                event.setDeathMessage(event.getDeathMessage().replaceAll(S, ChatColor.RED + S + ChatColor.GRAY));
            }
            for (String S : Domination.Blue.Members) {
                if (event.getDeathMessage().contains(" " + S)) Attacker = Bukkit.getPlayer(S);
                event.setDeathMessage(event.getDeathMessage().replaceAll(S, ChatColor.BLUE + S + ChatColor.GRAY));
            }
            if (Attacker != null) {
                int PKills = 1;
                if (DeathAndRespawnEvent.Kills.containsKey(Attacker.getName())) {
                    PKills += DeathAndRespawnEvent.Kills.get(Attacker.getName());
                } 
                DeathAndRespawnEvent.Kills.put(Attacker.getName(), PKills);
                if (DeathAndRespawnEvent.Kills.containsKey(PN)) {
                    P.setTotalExperience(Kills.get(PN));
                    DeathAndRespawnEvent.Kills.remove(PN);
                } else {
                    P.setTotalExperience(0);
                }
                KillStreak(Attacker, PKills);
            }
        }
    }
    
    public static void KillStreak(Player P, int Kills) {
        String PN = P.getName();
        if (Kills == 5) {
            Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + PN + " is on a 5 kill/cap streak.");
        } else if (Kills == 7) {
            Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + PN + " is on a 7 kill/cap rampage!");
        } else if (Kills == 10) {
            Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + PN + " is dominating on a 10 kill/cap rampage!");
        } else if (Kills == 15) {
            Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + PN + " must be using hacks, they're on a 15 kill/cap rampage!");
        } else if (Kills > 19 && Kills%5 == 0) {
            if (Domination.getTeam(PN) == Domination.Red) {
                Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + "Give up " + ChatColor.DARK_BLUE + "Blue Team " + ChatColor.DARK_PURPLE + PN + " is on a " + Kills + " kill/cap rampage!");
            } else {
                Bukkit.broadcastMessage(Domination.This.TagNo + ChatColor.DARK_PURPLE + "Give up " + ChatColor.DARK_RED + "Red Team " + ChatColor.DARK_PURPLE + PN + " is on a " + Kills + " kill/cap rampage!");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        if (RramaGaming.gameInPlay.equals("Domination") && RramaGaming.getGamer(event.getPlayer().getName()).m()) {
            final Player P = event.getPlayer();
            String PN = P.getName();
            event.setRespawnLocation(Spawning(P));
            Gamer G = RramaGaming.getGamer(PN);
            DominationClass C = Domination.Classes.get((String)G.getMetaData("Domination-Class"));
            C.setPlayerInvertory(P);
            Bukkit.getScheduler().runTaskLater(Domination.This, new Runnable() {

                @Override
                public void run() {
                    P.setFoodLevel(7);
                }
                
            }, 20);
        }
    }
    
    public static Location Spawning(final Player P) {
        ArrayList<Location> Options = new ArrayList<>();
        Team T = Domination.getTeam(P.getName());
        for (Checkpoint C : Arrays.asList(Checkpoints.A, Checkpoints.B, Checkpoints.C)) {
            if (C.getTeamInPosetion() == T && !C.isBeingOpposed()) {
                Options.add(C.getSpawn());
            }
        }
        if (Options.isEmpty()) {
            Bukkit.getScheduler().runTaskLater(Domination.This, new Runnable() {

                @Override
                public void run() {
                    P.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 1));
                }
            }, 20);
            return RramaGaming.MapInPlay.getSpawn();
        } else {
            return Options.get(new Random().nextInt(Options.size()));
        }
    }
}
