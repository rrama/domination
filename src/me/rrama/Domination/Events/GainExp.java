package me.rrama.Domination.Events;

import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class GainExp implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerExpChange(PlayerExpChangeEvent event) {
        if (RramaGaming.gameInPlay.equals("Domination")) {
            if (RramaGaming.getGamer(event.getPlayer().getName()).m()) {
                event.getPlayer().setTotalExperience(0);
                event.setAmount(0);
            }
        }
    }
}
