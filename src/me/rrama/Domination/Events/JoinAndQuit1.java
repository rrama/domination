package me.rrama.Domination.Events;

import java.util.Random;
import me.rrama.Domination.Domination;
import me.rrama.Domination.DominationClass;
import me.rrama.Domination.Team;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinAndQuit1 implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        Player P = event.getPlayer();
        String PN = P.getName();
        Gamer G = RramaGaming.getGamer(PN);
        G.addMetaData("Domination-Class", Domination.DefaultClass);
        
        if (!RramaGaming.gameInPlay.equals("Domination")) return;
        
        if (RramaGaming.R == RoundState.InGame) {
            P.setGameMode(GameMode.ADVENTURE);
            Team ChosenTeam;
            try {
                ChosenTeam = Domination.getTeam(PN);
                ChosenTeam.BetratyingMembers.remove(PN);
            } catch (NullPointerException ex) {
                if (Domination.Red.Members.size() < Domination.Blue.Members.size()) {
                    ChosenTeam = Domination.Red;
                } else if (Domination.Blue.Members.size() < Domination.Red.Members.size()) {
                    ChosenTeam = Domination.Blue;
                } else if (Domination.Red.getScore() <  Domination.Blue.getScore()) {
                    ChosenTeam = Domination.Red;
                } else if (Domination.Blue.getScore() <  Domination.Red.getScore()) {
                    ChosenTeam = Domination.Blue;
                } else if (Domination.Red.BetratyingMembers.size() < Domination.Blue.BetratyingMembers.size()) {
                    ChosenTeam = Domination.Red;
                } else if (Domination.Blue.BetratyingMembers.size() < Domination.Red.BetratyingMembers.size()) {
                    ChosenTeam = Domination.Blue;
                } else {
                    if ((new Random()).nextInt(1) == 0) {
                        ChosenTeam = Domination.Red;
                    } else {
                        ChosenTeam = Domination.Blue;
                    }
                }
            }
            ChosenTeam.Members.add(PN);
            G.setTagColour(ChosenTeam.getTeamColour());
            DominationClass C = Domination.Classes.get((String) G.getMetaData("Domination-Class"));
            G.addMetaData("Domination-Upgrade", 0);
            C.setPlayerInvertory(P);
            event.setJoinMessage(ChosenTeam.getTeamColour() + PN + " joined the game.");
            P.teleport(DeathAndRespawnEvent.Spawning(P));
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player P = event.getPlayer();
        String PN = P.getName();
        try {
            Team team = Domination.getTeam(PN);

            team.Members.remove(PN);
            team.BetratyingMembers.add(PN);
            event.setQuitMessage(team.getTeamColour() + PN + " left the game.");
        } catch (NullPointerException ex) {}
    }
}