package me.rrama.Domination;

import me.rrama.RramaGaming.GamingMap;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Checkpoints {
    
    public static Checkpoint A, B, C;
    
    public static void CreateCheckpoints(final GamingMap Map) {
        Vector CheckpointA = (Vector)Map.getMetaData("Domination-CheckpointA");
        Location CheckpointASpawn = (Location)Map.getMetaData("Domination-CheckpointASpawn");
        A = new Checkpoint("A", CheckpointA, CheckpointASpawn);
        Vector CheckpointB = (Vector)Map.getMetaData("Domination-CheckpointB");
        Location CheckpointBSpawn = (Location)Map.getMetaData("Domination-CheckpointBSpawn");
        B = new Checkpoint("B", CheckpointB, CheckpointBSpawn);
        Vector CheckpointC = (Vector)Map.getMetaData("Domination-CheckpointC");
        Location CheckpointCSpawn = (Location)Map.getMetaData("Domination-CheckpointCSpawn");
        C = new Checkpoint("C", CheckpointC, CheckpointCSpawn);
    }
    
}
