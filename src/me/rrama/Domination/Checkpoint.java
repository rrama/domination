package me.rrama.Domination;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.material.Wool;
import org.bukkit.util.Vector;

public class Checkpoint {
    
    private ArrayList<String> PlayersInbound = new ArrayList<>();
    private int Capping = 0;
    private final String Name;
    private final Vector V;
    private final Location Spawn;
    private Team TeamInPosetion;
    
    public Checkpoint(final String Name, final Vector BV, final Location Spawn) {
        this.Name = Name;
        this.V = BV;
        this.Spawn = Spawn;
    }
    
    public Vector getPostion() {
        return V;
    }
    
    public Location getSpawn() {
        return Spawn;
    }
    
    public void addPlayerToInbound(String PN) {
        PlayersInbound.add(PN);
    }
    
    public void clearPlayersInbound() {
        PlayersInbound.clear();
    }
    
    public boolean isBeingOpposed() {
        return (Capping != 0);
    }
    
    public void CappingCheck() {
        int RedIn = 0, BlueIn = 0;
        for (String S : PlayersInbound) {
            if (Domination.getTeam(S) == Domination.Red) {
                RedIn++;
            } else {
                BlueIn++;
            }
        }
        //TODO: Optimize below.
        if (getTeamInPosetion() == null && (RedIn != 0 && BlueIn != 0) || (RedIn == 0 && BlueIn == 0)) {
            Capping = 0;
        } else if (getTeamInPosetion() == Domination.Blue && RedIn == 0) {
            Capping = 0;
        } else if (getTeamInPosetion() == Domination.Red && BlueIn == 0) {
            Capping = 0;
        } else if ((getTeamInPosetion() == Domination.Blue || getTeamInPosetion() == null) && BlueIn == 0) {
            Capping += RedIn;
            Capping16Check(Domination.Red);
        } else if ((getTeamInPosetion() == Domination.Red || getTeamInPosetion() == null) && RedIn == 0) {
            Capping += BlueIn;
            Capping16Check(Domination.Blue);
        } else {
            GiveExp();
        }
    }
    
    private void Capping16Check(Team T) {
        if (Capping > 16) {
            TeamInPosetion = T;
            Capping = 0;
            Bukkit.broadcastMessage(Domination.This.TagB + "The " + T.getTeamColour() + T.getName() + " team " + ChatColor.BLUE + "have captured Checkpoint " + Name + "!");
            ColourSorroundingWool();
        } else {
            GiveExp();
        }
    }
    
    public void GiveExp() {
        for (String S : PlayersInbound) { //Not needed if set to 0 as 0 maker is in TimerTasks.
            Player P = Bukkit.getPlayerExact(S);
            P.giveExp(Capping);
        }
    }
    
    public Team getTeamInPosetion() {
        return TeamInPosetion;
    }
    
    public void ColourSorroundingWool() {
        Bukkit.getScheduler().runTask(Domination.This, new Runnable() {

            @Override
            public void run() {
                World w = Bukkit.getWorlds().get(0);
                for (int x = getPostion().getBlockX() - 5; x < getPostion().getBlockX() + 5; x++) {
                    for (int y = getPostion().getBlockY() - 5; y < getPostion().getBlockY() + 5; y++) {
                        for (int z = getPostion().getBlockZ() - 5; z < getPostion().getBlockZ() + 5; z++) {
                            if (w.getBlockTypeIdAt(x, y, z) == 35) {
                                BlockState WoolBS = w.getBlockAt(x, y, z).getState();
                                Wool wool = (Wool) WoolBS.getData();
                                DyeColor DC = wool.getColor();
                                if (DC.equals(DyeColor.SILVER) || DC.equals(DyeColor.RED) || DC.equals(DyeColor.BLUE)) {
                                    wool.setColor(getTeamInPosetion().getTeamWoolColour());
                                    WoolBS.update();
                                }
                            }
                        }
                    }
                }
            }
            
        });
    }
}
