package me.rrama.Domination;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class DominationMaps {
    
    public static void InitializeMaps(final File F) {
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                String LS = S.nextLine();
                World W = Bukkit.getWorlds().get(0);
                if (!LS.startsWith("#")) {
                    final String[] LSA = LS.split(" \\| ");
                    Domination.This.Maps.add(LSA[0]);
                    GamingMap ThisMap = GamingMaps.GamingMaps.get(LSA[0]);
                    
                    String[] VCAS = LSA[1].split(", ");
                    Vector VCA = new Vector(Integer.valueOf(VCAS[0]), Integer.valueOf(VCAS[1]), Integer.valueOf(VCAS[2]));
                    ThisMap.addMetaData("Domination-CheckpointA", VCA);
                    String[] LCAS = LSA[2].split(", ");
                    Location LCA = new Location(W, Double.valueOf(LCAS[0]), Double.valueOf(LCAS[1]), Double.valueOf(LCAS[2]), Integer.valueOf(LCAS[3]), Integer.valueOf(LCAS[4]));
                    ThisMap.addMetaData("Domination-CheckpointASpawn", LCA);
                    String[] VCBS = LSA[3].split(", ");
                    Vector VCB = new Vector(Integer.valueOf(VCBS[0]), Integer.valueOf(VCBS[1]), Integer.valueOf(VCBS[2]));
                    ThisMap.addMetaData("Domination-CheckpointB", VCB);
                    String[] LCBS = LSA[4].split(", ");
                    Location LCB = new Location(W, Double.valueOf(LCBS[0]), Double.valueOf(LCBS[1]), Double.valueOf(LCBS[2]), Integer.valueOf(LCBS[3]), Integer.valueOf(LCBS[4]));
                    ThisMap.addMetaData("Domination-CheckpointBSpawn", LCB);
                    String[] VCCS = LSA[5].split(", ");
                    Vector VCC = new Vector(Integer.valueOf(VCCS[0]), Integer.valueOf(VCCS[1]), Integer.valueOf(VCCS[2]));
                    ThisMap.addMetaData("Domination-CheckpointC", VCC);
                    String[] LCCS = LSA[6].split(", ");
                    Location LCC = new Location(W, Double.valueOf(LCCS[0]), Double.valueOf(LCCS[1]), Double.valueOf(LCCS[2]), Integer.valueOf(LCCS[3]), Integer.valueOf(LCCS[4]));
                    ThisMap.addMetaData("Domination-CheckpointCSpawn", LCC);
                }
            }
            if (Domination.This.Maps.isEmpty()) {
                Bukkit.getLogger().warning((Domination.This.TagB + "No mapz in da DominationMaps file!!! Domination plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(Domination.This);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Domination.This.TagB + "Cannot find da DominationMaps file!!! Domination plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(Domination.This);
        }
    }
}